

var app = new Vue({
	el: '#app',
	data: {

		students: [ ],
		filter: "",
		displaySalary: true,
		currentStudent: { name: "", salary: 0 }

	},
	methods: {
		average: function() {
			let list = this.filteredStudents.map(s => s.salary)
			if (list.length == 0)
				return "--"
			else
				return Math.round(eval(list.join("+"))/list.length) // Somme vraiment moche
		},
		save: function() {
			Vue.set(this.currentStudent, "saved",true)
			setTimeout(() => {
				this.currentStudent.saved = false
			}, 500)
			console.log("saved")
		}

	},
	computed: {
		filteredStudents: function() {
			return this.students.filter((student) => {
				return student.name.indexOf(this.filter) != -1
			})
		},
	},
	mounted: function() {
		this.$http.get("/students").then(function(response) {
			this.students = response.body
		})
		filter.focus()
	}

})


