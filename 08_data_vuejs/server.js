


let express = require("express")
let app = express()
let MongoClient = require("mongodb").MongoClient;

const client = new MongoClient("mongodb://localhost:27017", 
	{ useNewUrlParser: true, useUnifiedTopology: true });

let db = null;
client.connect(err => {
	db = client.db("dim")
})


app.use("/js",express.static(__dirname + "/js"))
app.use("/css",express.static(__dirname + "/css"))

app.get("/", function(req, res) {
	res.sendFile(__dirname + "/index.html")
})

app.get("/students", function(req, res) {
	db.collection("students").find().toArray(function(err, documents) {
		res.json(documents)
	})

})


app.listen(1337)

