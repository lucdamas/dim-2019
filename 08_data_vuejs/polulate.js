

let MongoClient = require("mongodb").MongoClient;


const client = new MongoClient("mongodb://localhost:27017", 
	{ useNewUrlParser: true, useUnifiedTopology: true });

let db = null;
client.connect(err => {

	db = client.db("dim")

	db.collection("students")
		.deleteMany({  }).then((result) => {

		let promises = []

		let names = [
			"Camille", "Anthony",  "Germain", "Thiébault", "Josselin",
			 "Yohan", "Yoan", "Sébastien", "Alexis", "Romain", "Yannick","Luc" 
		]

		for(let name of names) {
			let o = {
				name: name,
				salary: 1013+Math.floor(Math.random()*5000)
			}
			promises.push(db.collection("students").insertOne(o))
		}


		Promise.all(promises).then(() => {
			console.log("OK")
			client.close()
		}).catch(() => { })

	}).catch(() => { })


})


