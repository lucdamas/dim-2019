


document.addEventListener("DOMContentLoaded", function() {

	let body = document.querySelector("body")

	function e(tag, parent, text=null, id=null,classs=null) {
		let element = document.createElement(tag)
		parent.appendChild(element)
		if (text)
			element.appendChild(document.createTextNode(text))
		if (id)
			element.id = id
		if (classs)
			element.classList.add(classs)
		return element
	}



	let chatDiv = e("div", body, null, "chat")
	let toolsDiv = e("div", chatDiv, null, "tools")
	let pseudoInput = e("input", toolsDiv, null, "pseudo")
	pseudoInput.value = "LD"
	let messageInput = e("input", toolsDiv, null, "message")
	messageInput.focus()
	let sendButton = e("Button", toolsDiv, "Send")
	let messagesDiv = e("div", chatDiv, null, "messages")

	let socket = io("http://10.100.97.129:8080/")

	sendButton.addEventListener("click", send)
	messageInput.addEventListener("keydown", function(event) {
		if (event.keyCode == 13)
			send()
	})

	function send() {
		socket.emit("message",
		{ pseudo: pseudoInput.value, text: messageInput.value })
		messageInput.value = ""
	}

	socket.on("message", function(data) {
		let receivedDiv = e("div",messagesDiv, null, null,"received")
		let pseudoP = e("p",receivedDiv, data.pseudo, null, "sender")
		let textP = e("p",receivedDiv, data.text, null, "receivedText")

		messagesDiv.scrollTop = messagesDiv.scrollHeight
	})




	let canvas = e("canvas", body)
	canvas.width = window.innerWidth
	canvas.height = window.innerHeight

	let c = canvas.getContext("2d")

	let draw = false
	canvas.addEventListener("mousedown", function() { draw = true })
	canvas.addEventListener("mouseup", function() {	draw = false })

	canvas.addEventListener("mousemove", function(event) {
		if (draw)
			socket.emit("dot", {
				x: event.pageX / canvas.width,
				y: event.pageY / canvas.height,
				color: "rgba(255,127,0, 0.5)"
			})
	})

	socket.on("dot", function(point) {
		c.beginPath()
		c.arc(
			point.x*canvas.width,  
			point.y*canvas.height,
			5, 0, Math.PI*2
		)
		c.fillStyle = point.color
		c.fill()
	})




})