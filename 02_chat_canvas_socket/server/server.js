
let express = require("express")
let socketio = require("socket.io")
let http = require("http")

let app = express()
let server = http.Server(app)
let io = socketio(server)

app.use("/css", express.static(__dirname + "/css"))
app.use("/js", express.static(__dirname + "/client"))

app.get("/", function(req, res) {
    res.end("hop")
})

io.on("connection", function(socket) {

    // console.log("Connexion!")

   socket.on("message", function(data) {
   		if (data.pseudo && data.text &&
   			data.pseudo != "" && data.text != "") {
	   		
	   		data.date = new Date()
	    	io.emit("message", data)
			console.log(data)

   		}
   })
   socket.on("dot", function(data) {
   	io.emit("dot",data)
   })




})


server.listen(8080)
