<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Bières</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<header>
		<h1>Bières</h1>
		<form method='post' action='admin' id='admin'>
			<input type='submit' value='admin' />
			<?php include "uiConnection.php"; ?>
		</form>
	</header>
	<nav>
		<ul>
			<li><a href='#def'>Définition</a></li>
			<li><a href='#list'>Quelques bières</a></li>
		</ul>
	</nav>

	<section>
		<article id='def'>
			<h2>Définition</h2>				
			<div class='horiz'>
				<div>
				<picture>
					<img src="images/beer-def.jpg" alt='La bière'/>
				</picture>
					<p>La bière est une boisson alcoolisée obtenue par fermentation alcoolique d'un moût de produits végétaux amylacés tels que l'orge, le maïs, le riz (saké), la banane, le manioc... Ce moût est obtenu à l'issue d'une étape importante de la fabrication de la bière, le brassage, opération à l'origine des vocables brasseur et brasserie. C'est la plus ancienne boisson alcoolisée connue au monde et la boisson la plus consommée après l'eau et le thé.</p>
					<p>La bière actuelle (à partir du Moyen Âge) est généralement produite à partir d’eau, de malt d'orge (parfois additionnée d'autres céréales). Le houblon, en particulier, apporte un parfum et de l'amertume à la bière et agit comme conservateur. Cette boisson est consommée à la pression, en bouteille ou en canette. La consommation de bière est à l'origine de nombreux évènements festifs tels que la Fête de la bière, le Mondial de la bière, la Journée internationale de la bière ; et suscite également de nombreux jeux à boire comme le bière-pong, le Flip cup (en), le bong à bière (en), la botte de bière (de) ou le barathon. Des versions très faiblement alcoolisées (variant de 2° à 0°) sont présentes sur le marché. Contrairement aux autres boissons « sans alcool », elles sont élaborées par les mêmes procédés que la bière classique. La production de bière est réalisée industriellement ou artisanalement dans une brasserie, tout en étant possible par le particulier. </p>
				</div>
			</div>
		</article>
		<article id='list'>
			<h2>Quelques bières</h2>
			<ul>
				<li class='brune'>
					<span class='name'>Chimay bleue</span>
					<span class='country'>Belgique</span>
					<span class='color'>Brune</span>
					<span class='degree'>9°</span>
				</li>
				<li class='rousse'>
					<span class='name'>Rousse du Mont-Blanc</span>
					<span class='country'>Savoie</span>
					<span class='color'>Rousse</span>
					<span class='degree'>5°</span>
				</li>
				<li class='blanche'>
					<span class='name'>Bière de rivière d'ain</span>
					<span class='country'>France</span>
					<span class='color'>Blanche</span>
					<span class='degree'>5.4°</span>
				</li>
				<li class='blonde'>
					<span class='name'>Duvel</span>
					<span class='country'>Belgique</span>
					<span class='color'>Blonde</span>
					<span class='degree'>9°</span>
				</li>


			</ul>
		</article>
	</section>

	<footer>
		CC BY NC SA - LD 2019
	</footer>


	<script src='js/main.js'></script>
</body>
</html>

