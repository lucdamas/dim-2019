

document.addEventListener("DOMContentLoaded", function() {

	let button = document.querySelector("#admin input[type=submit]")	
	button.addEventListener("click", function(event) {
		document.querySelector("#background").classList.add("active")
		event.preventDefault();
	})

	let code = []
	let isClicking = false

	let dots = document.querySelectorAll(".dot")
	dots.forEach(function(oneDot, index) {
		oneDot.addEventListener("mousedown", function(event) {
			event.preventDefault()
			isClicking = true
			code.push(index)
			oneDot.classList.add("chosen")
		})
		oneDot.addEventListener("mouseenter", function() {
			if (isClicking) {
				code.push(index)
				oneDot.classList.add("chosen")
			}
		})

	})
	document.querySelector("#background").addEventListener("mouseup", function() {
		isClicking = false
		fetch("login.php?code="+code.join(""))
			.then(function(response) {
				return response.json()
			}).then(function(data) {
				// FAIRE JOLI AVANT
				for(let chosen of document.querySelectorAll(".chosen"))
					if (data.response)
						chosen.classList.add("good")
					else
						chosen.classList.add("bad")
				setTimeout(function() {
					// VERIFIER ET EFFACER
					if (data.response)
						document.querySelector("#admin").submit()
					for(let oneDot of dots) {
						oneDot.classList.remove("chosen")
						oneDot.classList.remove("bad")
						oneDot.classList.remove("good")
					}
					code = []
				}, 1000)
			})


	})



})
