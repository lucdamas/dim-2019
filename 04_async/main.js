
/*

PROCESSUS 

SYNCHRONE -> Attendre une réponse (Bloquante = pas bien)
- AJAX y'a longtemps : interdit
- wait : pas bien

Code qui verrouille le navigateur (pas bien)

document.addEventListener("DOMContentLoaded", function() {

	console.log("hop")

	function wait(milliseconds) {
		var start = new Date().getTime();
		while (new Date().getTime() - start < milliseconds) { }
	}

	document.querySelector("button").addEventListener("click", function() {
		console.log("click")
		document.querySelector("img").src = "chocolate.jpg"
	})

	wait(5000)


})


ASYNCHRONE -> Gestion d'événement de fin de PROCESSUS
- setTimeout -> ne fait rien puis déclenche une action
- AJAX -> connection HTTP et attente de réponse
- Evénements divers : attente d'un clic, loaded, ...


-> CALLBACK
-> PROMISE    .then
-> ASYNC/AWAIT = promise cachée

*/





// AVEC DES CALLBACK

// document.addEventListener("DOMContentLoaded", function() {

// 	function drinkPlease(callback) {
// 		setTimeout(function() {
// 			callback("Café")
// 		}, 1000)
// 	}

// 	drinkPlease(function(drink) {
// 		console.log(drink)
// 	})

	
// 	function cutVegetables(callback) {
// 		setTimeout(function() {
// 			console.log("Vegetable ready")
// 			callback()
// 		}, 1000)
// 	}
// 	function cutFish(callback) {
// 		setTimeout(function() {
// 			console.log("Nemo cutted")
// 			callback()
// 		}, 1000)
// 	}
// 	function cookRice(callback) {
// 		setTimeout(function() {
// 			console.log("Rice cooked")
// 			callback()
// 		}, 1000)
// 	}
// 	function prepareRiceSauce(callback) {
// 		setTimeout(function() {
// 			console.log("Vinegar and sugar ok")
// 			callback()
// 		}, 1000)
// 	}
// 	function rollEverything(callback) {
// 		setTimeout(function() {
// 			console.log("Ready!")
// 			callback()
// 		}, 1000)
// 	}

// 	// CALLBACK HELL

// 	cutVegetables(function(vegetables) {
// 		cutFish(function(fish) {
// 			cookRice(function(rice) {
// 				prepareRiceSauce(function(sauce) {
// 					rollEverything(function(sushis) {
// 						console.log("Miam!")
// 					})
// 				})
// 			})
// 		})
// 	})
	

// })






// document.addEventListener("DOMContentLoaded", function() {


// 	function apod() {
// 		return new Promise(function(resolve) {
// 			let apodUrl = "https://api.nasa.gov/planetary/apod?api_key=FoTMpaohHa1vrXayCvprXbOgyjfSCCowPqhc1syG"
// 			$.get(apodUrl, {}, function(data) {
// 				resolve(data)
// 				document.querySelector("#title").innerHTML = data.title
// 				document.querySelector("#explanation").innerHTML = data.explanation
// 				document.querySelector("#apodImg").src = data.url

// 			}, "json")
// 		})
// 	}

// 	function message() {
// 		return new Promise(function(resolve) {
// 			$.get("service.php", { }, function(data) {
// 				resolve(data)
// 			}, "json")
// 		})
// 	}


// 	Promise.all([ apod(), message() ])
// 		.then(function(data) {
// 			// DATA = données de chacun des service
// 			console.log(data)
// 			console.log("Terminé")
// 		})
// })


// ------------------------------------------------------
// Pipotron 
// Chaque partie est aléatoire
// - Sujet : sujet.php (100 - 1000 ms)
// - Verbe : verbe.php (200 - 800 ms)
// - Complément : complement.php (400 - 2000 ms)


async function sujet() {
	return new Promise(function(resolve) {
		$.get("pipotron.php", {  action: "sujet"},
			function(data) {
				resolve(data)
			}, "json")
	})
}
async function verbe() {
	return new Promise(function(resolve) {
		$.get("pipotron.php", {  action: "verbe"},
			function(data) {
				resolve(data)
			}, "json")
	})
}
async function complement() {
	return new Promise(function(resolve) {
		$.get("pipotron.php", {  action: "sujet"},
			function(data) {
				resolve(data)
			}, "json")
	})
}

// Promise.all([ sujet(), verbe(), complement()])
// 	.then(function(phrase) {
// 		console.log(
// 			phrase[0].sujet + " " +
// 			phrase[1].verbe + " " +
// 			phrase[2].sujet + "."
// 		)
// 	})


async function pipotron() {

	// Syntaxe moche avec dépendance des appels
	sujet().then(function(s) {
		verbe(s).then(function(v) {
			complement().then(function(c) {
				console.log(s.sujet+" "+v.verbe+" "+c.sujet)
			})
		})
	})

	// Syntaxe allégée avec dépendance des appels
	s = await sujet()
	v = await verbe(s)
	c = await complement()

	console.log(s.sujet+" "+v.verbe+" "+c.sujet)


}
pipotron()




// CODE DE JOSSELIN

// document.addEventListener("DOMContentLoaded", (event) => {
//     Promise.all([getSubject(), getVerb(), getComplement()])
//         .then(function(data) {
// 	        console.log(data) // pas affiché
//         })
// });

// function getVerb() {
//     return new Promise(function(resolve){
//         $.get("pipotron.php", {action: "verbe"}, function(data) {
// 	    	console.log("verbe")
//             resolve(data)
//         }, "json")
//     });
// }

// function getSubject() {
//     return new Promise(function(resolve){
//         $.get("pipotron.php", {action: "sujet"}, function(data) {
// 	    	console.log("sujet")
//             resolve(data)
//         }, "json")
//     });
// }

// function getComplement() {
//     return new Promise(function(resolve){
//         $.get("pipotron.php", {action: "sujet"}, function(data) {
// 	    	console.log("complement")
//             resolve(data)
//         }, "json")
//     });
// }

