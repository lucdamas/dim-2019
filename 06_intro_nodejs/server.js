

const express = require("express")
const app = express()
app.use(express.json())

let MongoClient = require("mongodb").MongoClient;


const client = new MongoClient("mongodb://localhost:27017", 
	{ useNewUrlParser: true, useUnifiedTopology: true });
let db = null;
client.connect(err => {
	db = client.db("movies")
})


app.use("/js",express.static(__dirname + "/js"))
app.use("/css",express.static(__dirname + "/css"))

app.get("/", function(req, res) {
	res.sendFile(__dirname + "/index.html")
})

app.get("/movies", function(req, res) {
	db.collection("movies").find().toArray(function(err, documents) {
		res.json(documents)
	})

})

app.post("/add", function(req, res) {

	let movie = req.body
	movie.id = Math.floor(Math.random()*10000)

   db.collection("movies").insertOne(movie,
                function(err, result) {
					res.json({ id: movie.id })
                })

})

app.post("/del", function(req, res) {

	db.collection("movies").deleteOne({ id: req.body.id }, function() {
		res.json({ del: "ok" })
	})

})

app.post("/update", function(req, res) {


	db.collection("movies").updateOne(
		{ id : req.body.id },
		{ $set: req.body })
	.then(
		function(err) {
			res.json({ update: "ok" })
		})


})




app.listen(1337)
