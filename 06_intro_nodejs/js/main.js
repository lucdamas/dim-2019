


document.addEventListener("DOMContentLoaded", function() {

	let table = document.querySelector("#movies")
	let form = document.querySelector("#form")
	let addButton = form.querySelector("#addButton")
	let udpateButton = form.querySelector("#updateButton")
	udpateButton.classList.add("disabled")
	let updating = false
	let updatedRow = null

	axios.get("/movies").then(function(response) {
		for(let movie of response.data) 
			displayMovie(movie)
	})

	function displayMovie(movie) {
		let tr = e("tr", null, table)
		let tdTitle = e("td", movie.title, tr, "title")
		let tdDirector = e("td", movie.director, tr, "director")
		let tdYear = e("td", movie.year, tr, "year")
		let tdCateg = e("td", movie.categories.join(", "), tr, "categories")
		let tdDel = e("td", null, tr)
		let delButton = e("button", "X", tdDel)
		delButton.addEventListener("click", function() {
			axios.post("/del", { id: movie.id }).then(function(response) {
				console.log(response.data)
				tr.remove()
			})
		})
		tr.addEventListener("dblclick", function() {
			form.querySelector("#id").value = movie.id
			form.querySelector("#title").value = movie.title
			form.querySelector("#director").value = movie.director
			form.querySelector("#year").value = movie.year
			form.querySelector("#categories").value = movie.categories
			updating = true
			updatedRow = tr
			udpateButton.classList.remove("disabled")
			addButton.classList.add("disabled")

		})
	}

	//------------------------------------------------------
	addButton.addEventListener("click", function() {

		let movie = {
			title: form.querySelector("#title").value,
			director: form.querySelector("#director").value,
			year: parseInt(form.querySelector("#year").value),
			categories: form.querySelector("#categories").value.split(", ")
		}

		axios.post("/add", movie).then(function(response) {
			movie.id = response.data.id
			displayMovie(movie)
			form.querySelector("#title").value = ""
			form.querySelector("#director").value = ""
			form.querySelector("#year").value = ""
			form.querySelector("#categories").value = ""
		})

	})

	//------------------------------------------------------
	updateButton.addEventListener("click", function() {



		let movie = {
			id: parseInt(form.querySelector("#id").value),
			title: form.querySelector("#title").value,
			director: form.querySelector("#director").value,
			year: parseInt(form.querySelector("#year").value),
			categories: form.querySelector("#categories").value.split(", ")
		}

		axios.post("/update", movie).then(function(response) {

			updatedRow.querySelector(".title").innerText = movie.title
			// Trop relou de tout modifier (tableau + objet)



			form.querySelector("#title").value = ""
			form.querySelector("#director").value = ""
			form.querySelector("#year").value = ""
			form.querySelector("#categories").value = ""
			updating = false
			udpateButton.classList.add("disabled")
			addButton.classList.remove("disabled")

		})


	})




})






function e(tag, text, parent, classs=null, id=null) {
	let o = document.createElement(tag)
	if (text != null)
		o.appendChild(document.createTextNode(text))
	if (classs != null)
		o.classList.add(classs)
	if (id != null)
		o.id = id
	parent.appendChild(o)
	return o
}














