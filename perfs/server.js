

const express = require("express")
const app = express()
app.use(express.json())
let MongoClient = require("mongodb").MongoClient;


const client = new MongoClient("mongodb://localhost:27017", 
	{ useNewUrlParser: true, useUnifiedTopology: true });
let db = null;
client.connect(err => {
	db = client.db("perfs")
})


app.use("/js",express.static(__dirname + "/js"))
app.use("/css",express.static(__dirname + "/css"))

app.get("/", function(req, res) {
	res.sendFile(__dirname + "/index.html")
})

app.get("/data", function(req, res) {
	db.collection("data").find().toArray(function(err, documents) {
		res.json(documents)
	})

})




app.listen(1337)
