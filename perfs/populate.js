

const express = require("express")
const app = express()
app.use(express.json())
let MongoClient = require("mongodb").MongoClient;


const client = new MongoClient("mongodb://localhost:27017", 
	{ useNewUrlParser: true, useUnifiedTopology: true });
let db = null;
client.connect(err => {
	db = client.db("perfs")

	db.collection("data").deleteMany({  }).then((result) => {

		let promises = []
		for(let i=0; i<10000; i++) {
			let o = { }
			for(let j=0; j<10; j++) {
				o["x"+j] = Math.random()
			}
			promises.push(db.collection("data").insertOne(o))
		}


		Promise.all(promises).then(() => {
			console.log("OK")
			client.close()
		})

	})


})


