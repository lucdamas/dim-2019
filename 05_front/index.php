<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

	<header>
		<h1>Lorem Ipsum</h1>
	</header>

	<div id='banner'>
		<?php
		for($i=0; $i<100; $i++) {
			$r = rand(0,100);
			$class = $r<80?"small":($r>90?"big":"medium");
			echo "<div class='bubble $class'></div>";
		}
		?>
	</div>

	<section>
		<article>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vestibulum enim non augue laoreet, at cursus eros efficitur. Pellentesque facilisis, purus quis fringilla lobortis, mauris lorem feugiat sem, vitae fermentum neque velit quis nisl. Maecenas venenatis sapien et libero efficitur, vitae vestibulum urna semper. Integer finibus egestas purus, at molestie elit accumsan eu. Integer lobortis nec nunc quis vulputate. Nulla facilisi. Sed non interdum metus, a pharetra ipsum. Maecenas molestie ut ligula a vulputate. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec quis eros sed nisi porttitor eleifend. Fusce pretium elementum orci, vel blandit dolor tempor id. Aliquam non aliquam turpis. Integer feugiat dignissim tellus non ullamcorper. Donec vel est et sapien vulputate rhoncus.</article>
		<article>Sed a feugiat lacus, ac tempus eros. Ut tempor laoreet tortor, nec varius sapien rutrum nec. Fusce ullamcorper odio magna, nec dapibus diam sollicitudin in. Curabitur sit amet accumsan ipsum. Ut at ante ipsum. Quisque ullamcorper semper odio, eget tempus mi congue id. Nullam iaculis, mauris sed dignissim mollis, tellus tellus hendrerit diam, eget scelerisque massa nulla et mi. Aliquam dictum nisl non suscipit dictum. Integer elementum pellentesque purus nec accumsan.</article>
		<article>Morbi euismod hendrerit est, ac tempus risus tempor in. Nulla facilisi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras eu egestas neque, quis lacinia augue. Donec rutrum sollicitudin dui ultricies pulvinar. Pellentesque commodo laoreet cursus. Ut in tortor vel leo convallis porttitor vitae ac sapien. Mauris eget pretium nunc. Phasellus nec feugiat risus, non egestas dolor. Sed congue ac lorem ac pulvinar. Cras non sagittis enim. Morbi quis nulla vitae mauris iaculis molestie ac a lectus.</article>
		<article>Cras vel ipsum lectus. Proin lacinia ut quam id ornare. Sed arcu lectus, imperdiet sed suscipit quis, semper sed enim. Fusce auctor libero nunc, vitae imperdiet purus pellentesque eu. Etiam nec tortor quis libero hendrerit iaculis. Maecenas ac bibendum leo. Ut ut mi sagittis, pretium massa at, dignissim odio. Vestibulum vitae porta nisi. Suspendisse sem neque, tempus a diam vitae, tempor vehicula nunc. Sed a mauris sem. Proin imperdiet purus a diam sagittis, a scelerisque sapien consequat. Maecenas convallis maximus tortor nec condimentum. Duis viverra, ex ut scelerisque mollis, libero sem ullamcorper sapien, nec viverra mi turpis lacinia arcu. Nunc in justo purus. Vestibulum at faucibus massa. Proin ac scelerisque lorem, quis iaculis neque.</article>
		<article>Nunc sed auctor nibh. Aliquam consequat auctor diam sed pretium. Quisque lectus sapien, convallis quis ultricies in, ultrices at quam. Fusce dictum odio orci, eu maximus diam bibendum ut. Nullam blandit dictum sagittis. Etiam faucibus consequat nibh pretium semper. Donec quis semper libero, id faucibus felis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer porta eget turpis et scelerisque. Donec iaculis id magna ut vulputate. Nullam hendrerit risus est, a placerat lectus interdum convallis. Vivamus suscipit, libero sit amet sodales ultrices, orci dui volutpat ipsum, sit amet suscipit ipsum nibh in sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; </article>
	</section>




	<script src='main.js'></script>
</body>
</html>