
document.addEventListener("DOMContentLoaded", function() {


	let html = document.querySelector("html")
	let body = document.querySelector("body")
	let banner = document.querySelector("#banner")

	let bannerRect = banner.getBoundingClientRect()

	// placement des bulle au hasard dans la banner
	let bubbles = document.querySelectorAll(".bubble")
	for(let bubble of bubbles) {
		let size = bubble.getBoundingClientRect().width
		bubble.style.left = (Math.random()*
			              (bannerRect.width-size))+"px"
		bubble.style.top = (Math.random()*
			              (bannerRect.height-size))+"px"
		bubble.speed = { x: 0, y: 0 }
	}

	// Evenement et récup de la souris
	let m = { x: 0, nx: 0, y: 0 }
	body.addEventListener("mousemove", function(e) {
		m.nx = e.pageX / bannerRect.width // X normalisé
		m.x = e.pageX
		m.y = e.pageY
	})


	// Animation
	function bannerAnimate(timestamp) {
		for(let bubble of bubbles) {
			let rect = bubble.getBoundingClientRect()
			bannerRect = banner.getBoundingClientRect()

			// Calcul de la vitesse
			bubble.speed.x = 10*(m.nx - 0.5)/rect.width + html.scrollTop/100
			bubble.speed.y = 2*Math.cos(timestamp/1000)/rect.width

			let y = rect.top+bubble.speed.y-bannerRect.top
			let x = rect.left+bubble.speed.x
			if (x > bannerRect.width)
				x = -rect.width
			else if (x < -rect.width)
				x = bannerRect.width

			bubble.style.top = y+"px"
			bubble.style.left = x+"px"
		}
		window.requestAnimationFrame(bannerAnimate)
	}
	window.requestAnimationFrame(bannerAnimate)

	let section = document.querySelector("section")
	let articles = section.querySelectorAll("article")
	let nbScroll = 0
	section.addEventListener("wheel", scroll)

	console.log(articles.length)

	function scroll(event) {
		console.log(nbScroll)
		event.preventDefault()
		section.removeEventListener("wheel", scroll)
		let dir = event.deltaY > 0?-1:1
		if ((dir > 0 && nbScroll!=0) || 
			(dir < 0 && nbScroll>-articles.length+1))
			nbScroll += dir
		section.style.marginLeft = (nbScroll*100)+"vw"
		setTimeout(function() {
			section.addEventListener("wheel", scroll)
		}, 600)

	}






})