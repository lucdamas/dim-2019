		// GET : appel AJAX = requête HTTP au serveur
		// param 1 : url 
		// param 2 : données envoyées au serveur
		// param 3 : callback = fct appelée quand le travail est terminé	
		// param 4 : type de réponse (json, html)

		// let article = e("article", "Fait ou ne fait pas, mais il n'y a pas d'essai", list)


document.addEventListener("DOMContentLoaded",

	function() {

		let list = document.querySelector("#list")
		//-- factorisation
		function displayProverb(proverb) {			
			let article = e("article", null, list)
			let paragraph = e("p", proverb.value, article)
			let edit = e("input", null, article)
			edit.value = proverb.value
			edit.classList.add("hidden")
			let suppr = e("button", "X", article)
			suppr.addEventListener("click", function() {
				$.get(
					"service.php",
					{ action: "delete", id: proverb.id },
					function(response) {
						console.log(response)
						article.remove()
					},
					"json"
				)
			})
			paragraph.addEventListener("dblclick", function() {
				edit.classList.remove("hidden")
				edit.focus()
				paragraph.classList.add("hidden")
			})
			edit.addEventListener("keyup", function(event) {
				if (event.code == "Escape") {
					edit.classList.add("hidden")
					paragraph.classList.remove("hidden")					
				} 
				else if (event.code == "Enter") {
					$.get(
						"service.php",
						{ action: "update", 
						  id: proverb.id,
						  value: edit.value
						},
						function(response) {
							proverb.value = edit.value
							paragraph.textContent = proverb.value
							edit.classList.add("hidden")
							paragraph.classList.remove("hidden")					
						},
						"json"
					)

				}

			})
		}
		//-- Chargement initial et affichage
		$.get(
			"service.php",
			{ action: "list" },
			function(response) {
				for(let proverb of response) {
					displayProverb(proverb)
				}
			},
			"json"
		)
		//-- Ajout
		document.querySelector("#tools button")
			.addEventListener("click", function() {
				let proverb = document.querySelector("#proverb").value
				$.get(
					"service.php",
					{ action: "add", value: proverb },
					function(response) {
						document.querySelector("#proverb").value = ""
						displayProverb(response)
					},
					"json"
				)




			})


	})




function e(tag, text, parent, classs=null, id=null) {
	let o = document.createElement(tag)
	if (text != null)
		o.appendChild(document.createTextNode(text))
	if (classs != null)
		o.classList.add(classs)
	if (id != null)
		o.id = id
	parent.appendChild(o)
	return o
}





